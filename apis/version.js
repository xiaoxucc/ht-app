import $http from '@/utils/request.js'

/**
 * GET
 * /api/SubDev/tversions/GetDomainName
 * 获得最新域名
 */
export const getDomainName = () => {
	return $http.request({
		url: `/api/SubDev/tversions/GetDomainName`,
		method: 'GET'
	})
}