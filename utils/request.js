export default {
	common: {
		baseUrl: 'http://47.108.232.115:13500',
		data: {},
		header: {
			'Authorization': uni.getStorageSync('token') || '',
		},
		method: 'GET',
		timeout: 10000,
		dataType: 'json',
	},
	request(options = {}) {
		if (options.loading) {
			uni.showLoading({
				title: options.loadingTitle || '加载中',
				mask: options.loadingMask || false
			});
		}
		options.url = this.common.baseUrl + options.url
		options.data = options.data || this.common.data
		options.header = options.header || this.common.header
		options.method = options.method || this.common.method
		options.dataType = options.dataType || this.common.dataType
		options.header['Content-Type'] = options.method === 'GET' ? 'application/json' : 'application/x-www-form-urlencoded'
		return new Promise((resolve, reject) => {
			uni.request({
				...options,
				success: result => {
					if (result.statusCode !== 200 || result.data.code !== 200) {
						uni.showModal({
							title: '提示',
							content: result.data.msg,
							showCancel: false,
							confirmText: '我知道了',
						})
						reject(result.data)
					}
					resolve(result.data)
				},
				fail: error => {
					uni.showModal({
						title: '提示',
						content: error,
						showCancel: false,
					})
					reject(error)
				},
				complete: () => {
					if (options.loading) {
						uni.hideLoading()
					}
				}
			})
		})
	},
}